package com.dennismok2018.prototype.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.dennismok2018.prototype.domain.Product;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDto {

        @JsonProperty(value = "pid")
        private Long id;

        private String name;

        private String description;

        @JsonProperty(value = "image_url")
        private String imageUrl;

        private BigDecimal price;

        private Integer stock;


        public ProductDto(Product product) {
            this.id = product.getId();
            this.name = product.getName();
            this.description = product.getDescription();
            this.imageUrl = product.getImageUrl();
            this.price = product.getPrice();
            this.stock = product.getStock();
        }


}
