package com.dennismok2018.prototype.domain.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;


@Data
@Table(value = "product")
public class ProductEntity {

    @Column(value = "pid")
    @Id
    private Long id;

    private String name;

    private String description;

    @Column(value = "image_url")
    private String imageUrl;

    private BigDecimal price;

    private Integer stock;

}
