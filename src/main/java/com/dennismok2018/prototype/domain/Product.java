package com.dennismok2018.prototype.domain;


import com.dennismok2018.prototype.domain.entity.ProductEntity;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {

    private Long id;
    private String name;
    private String description;
    private String imageUrl;
    private BigDecimal price;
    private Integer stock;

    public ProductEntity toEntity(){
        ProductEntity entity = new ProductEntity();
        entity.setId(id);
        entity.setName(name);
        entity.setDescription(description);
        entity.setImageUrl(imageUrl);
        entity.setPrice(price);
        entity.setStock(stock);
        return entity;
    }

    public Product(ProductEntity entity){
        this.id = entity.getId();
        this.name = entity.getName();
        this.description = entity.getDescription();
        this.imageUrl = entity.getImageUrl();
        this.price = entity.getPrice();
        this.stock = entity.getStock();
    }

}
