package com.dennismok2018.prototype.repository;

import com.dennismok2018.prototype.domain.entity.ProductEntity;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface ProductRepository extends ReactiveCrudRepository<ProductEntity, Long> {

    @Query(value = "Select * from `product`")
    Flux<ProductEntity> getAll();

    @Query(value = "Select * from `product`\n" +
            "WHERE `pid` = :pid")
    Mono<ProductEntity> getProductById(Long pid);

}
