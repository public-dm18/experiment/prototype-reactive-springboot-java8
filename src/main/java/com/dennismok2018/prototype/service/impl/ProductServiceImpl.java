package com.dennismok2018.prototype.service.impl;

import com.dennismok2018.prototype.domain.Product;
import com.dennismok2018.prototype.domain.entity.ProductEntity;
import com.dennismok2018.prototype.repository.ProductRepository;
import com.dennismok2018.prototype.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;


    @Override
    public List<Product> getAllProducts() throws ExecutionException, InterruptedException {

       List<Product> products = productRepository.getAll().map(e -> new Product(e)).collectList().toFuture().get();

       return products;

    }

    @Override
    public Product getProductById(Long id) throws ExecutionException, InterruptedException {
        return productRepository.getProductById(id).map(e -> new Product(e)).toFuture().get();
    }
}
