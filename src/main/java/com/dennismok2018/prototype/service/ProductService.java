package com.dennismok2018.prototype.service;

import com.dennismok2018.prototype.domain.Product;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;


public interface ProductService {

    List<Product> getAllProducts() throws ExecutionException, InterruptedException;

    Product getProductById(Long id) throws ExecutionException, InterruptedException;

}
