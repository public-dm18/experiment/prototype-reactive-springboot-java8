package com.dennismok2018.prototype.controller;


import com.dennismok2018.prototype.domain.Product;
import com.dennismok2018.prototype.domain.dto.ProductDto;
import com.dennismok2018.prototype.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
@RequestMapping(value = "/public")
public class PublicProductApi {

    @Autowired
    ProductService productService;

    @GetMapping(value = "/product/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductDto> getAllProducts() throws ExecutionException, InterruptedException {
        log.info("Endpoint is called at" + new Date().getTime());

        List<ProductDto> dtos = new ArrayList<>();

        List<Product> products = productService.getAllProducts();

        for (Product p : products){
            dtos.add(new ProductDto(p));
        }
        return dtos;
    }

    @GetMapping(value = "/product/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductDto getAllById(@PathVariable Long id) throws ExecutionException, InterruptedException {
        log.info("Endpoint is called at" + new Date().getTime());

        Product product = productService.getProductById(id);

        if (product == null){
            return null;
        }

        return new ProductDto(product);
    }

}
